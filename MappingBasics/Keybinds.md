# Star Citizen 3.10 Keybinds

Default keybinds for mouse and keyboard in Star Citizen 3.10

# CH Control Manager / JoyToKey / TeamSpeak / etc.

Modifiers are set to a control key such as Alt, Ctrl or Shift. Star Citizen, CH Control Manager and JoyToKey can all specify the left or right Alt/Ctrl/Shift. The left version is more commonly used on the keyboard and in other games, the right can often be devoted to your joystick bindings without interfering with other normal keyboard and mouse bindings. 

Use push-to-talk for Discord, TeamSpeak, Ventrilo, Mumble, etc. with priority push-to-talk as an alternate binding for a command channel or a priority push-to-talk binding in applications like Discord.

## Modifiers
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Modifier Key 1 (M1)                          | Right Alt              |
| Modifier Key 2 (M2)                          | Right Ctrl             | 

## Voice
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Push-to-Talk                                 |                        | 
| Priority Push-to-Talk                        |                        | 
| Mute Sound                                   |                        | 

# Star Citizen

## Flight - Cockpit
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Eject                                        | Y                      |
| Exit Seat                                    | Y                      |
| Self Destruct                                | Backspace              |
| Emergency Exit Seat                          | Left Shift + U         |
| Increase Cooler Rate                         |                        |
| Decrease Cooler Rate                         |                        |
| Flight / Systems Ready                       | R                      |
| Open/Close Doors (Toggle)                    | K                      |
| Open All Doors                               |                        |
| Close All Doors                              |                        |
| Lock/Unlock Doors (Toggle)                   | L                      |
| Lock All Doors                               |                        |
| Unlock All Doors                             |                        |

## Flight - View
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Look left                                    |                        |
| Look right                                   |                        |
| Look left / right                            | X-Axis (mouse)         |
| Look up                                      |                        |
| Look down                                    |                        |
| Look up / down                               | Y-Axis (mouse)         |
| Cycle camera view                            | F4                     |
| Cycle camera orbit mode                      |                        |
| Zoom in (3rd person)                         | Mouse Wheel Up         |
| Zoom out (3rd person)                        | Mouse Wheel Down       |
| Freelook (Hold)                              | Z                      |
| Dynamic zoom in and out (rel.)               | Left Alt + Mouse Wheel |
| Dynamic zoom in (rel.)                       |                        |
| Dynamic Zoom Toggle (abs.)                   |                        |
| Look behind                                  |                        |

## Flight - Movement

### Directional
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Pitch up                                     |                        |
| Pitch down                                   |                        |
| Pitch                                        | Y-Axis (mouse)         |
| Yaw left                                     |                        |
| Yaw right                                    |                        |
| Yaw                                          | X-Axis (mouse)         |
| Cycle mouse mode                             |                        |
| Roll left                                    | Q                      |
| Roll right                                   | E                      |
| Roll                                         |                        |
| Swap Yaw / Roll (Toggle)                     |                        |
| Lock Pitch / Yaw Movement (Toggle, Hold)     | Right Shift            |
| E.S.P. (Toggle)                              |                        |

### Limiters
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Speed Limiter Increase                       |                        |
| Speed Limiter Decrease                       |                        |
| Speed Limiter (rel.)                         | Mouse Wheel            |
| Speed Limiter (abs.)                         |                        |
| Acceleration Limiter Increase                |                        |
| Acceleration Limiter Decrease                |                        |
| Acceleration Limiter Up / Down (rel.)        | Right Alt + Mouse Wheel|
| Acceleration Limiter Up / Down (abs.)        |                        |
| Speed Limiter (Toggle)                       |                        |
| G-force safety (Toggle)                      |                        |

### Engines & Thrusters
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Afterburner                                  | Left Shift             | 
| Spacebrake                                   | X                      | 
| Throttle forward                             | W                      | 
| Throttle back                                | S                      | 
| Throttle forward / back                      |                        | 
| Strafe up                                    | Spacebar               | 
| Strafe down                                  | Left Ctrl              | 
| Strafe up / down                             |                        | 
| Strafe left                                  | A                      | 
| Strafe right                                 | D                      | 
| Strafe left / right                          |                        | 
| Strafe forward / back invert                 |                        | 
| VTOL (Toggle)                                | J                      | 
| Cruise Control (Toggle)                      | C                      | 
| Decoupled mode (Toggle)                      | V                      | 

### Quantum & Landing
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Quantum Travel System (Toggle)               | B                      | 
| Quantum Drive (Hold)                         | B                      | 
| Landing System (Toggle)                      | N                      | 
| Autoland (Hold)                              | N                      | 

## Flight - Targeting
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Lock / Unlock Selected Target                | 1                      |
| Lock Nearest Attacker                        | 2                      |
| Lock Next Pinned Target                      | 4                      |
| Lock Previous Pinned Target                  | 5                      |
| Unlock Locked Target                         |                        |

### Pinning
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Pin / Unpin Selected Target                  | 3                      |
| Pin Index 1 - Lock / Unlock Pinned Target    | Left Alt + 1           |
| Pin Index 2 - Lock / Unlock Pinned Target    | Left Alt + 2           |
| Pin Index 3 - Lock / Unlock Pinned Target    | Left Alt + 3           |
| Pin Index 1 - Pin / Unpin Selected Target    | Left Alt + 1           |
| Pin Index 2 - Pin / Unpin Selected Target    | Left Alt + 2           |
| Pin Index 3 - Pin / Unpin Selected Target    | Left Alt + 3           |
| Remove All Pinned Targets                    | 0                      |

### Selection
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Hostiles - Select Next Target in View        |                        |
| Hostiles - Select Previous Target in View    |                        |
| Friendlies - Select Next Target in View      |                        |
| Friendlies - Select Previous Target in View  |                        |
| All Targets - Select Next Target in View     |                        |
| All Targets - Select Previous Target in View |                        |
| Reset Selection                              |                        |

### Sub-Target
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Lock Next Sub-Target                         |                        |
| Lock Previous Sub-Target                     |                        |
| Reset Sub-Target                             |                        |

### Utility
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Enable / Disable Look Ahead                  | Left Alt + R           |
| Look at Locked Target (Toggle, Hold)         |                        |
| Auto Zoom on Selected Target (Toggle, Hold)  |                        |
| Switch Flight Lead / Lag Reticle (Toggle)    |                        |

### Scanning
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Scanning Mode (Toggle)                       | Tab                    |
| Activate Scanning                            | Button 1 (mouse)       |
| Scanning Radar Ping                          | Button 2 (mouse)       |
| Scanning Increase Radar Angle                | Mouse Wheel Up         |
| Scanning Decrease Radar Angle                | Mouse Wheel Down       |

## Flight - Weapons
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Fire Weapon Group 1                          | Button 1 (mouse)       |
| Fire Weapon Group 2                          | Button 2 (mouse)       |
| Cycle Weapon Ammo                            |                        |
| Cycle Weapon Ammo (Back)                     |                        |
| Gimbal Mode (Toggle, Hold)                   |                        |
| Cycle Gimbal Assist                          | R                      |
| Acquire missile lock (Tap)                   | Button 3 (mouse)       |
| Release missile lock (Tap)                   |                        |
| Launch missile (Hold)                        | Button 3 (mouse)       |

## Flight - Defensive
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Launch countermeasure                        | G                      |
| Cycle countermeasure ammo                    | H                      |
| Cycle countermeasure ammo (Back)             |                        |
| Shield raise level front                     | Numpad 8               |
| Shield raise level back                      | Numpad 2               |
| Shield raise level left                      | Numpad 4               |
| Shield raise level right                     | Numpad 6               |
| Shield raise level top                       | Numpad 7               |
| Shield raise level bottom                    | Numpad 9               |
| Shield reset levels                          | Numpad 5               |

## Turrets
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Pitch up                                     |                        |
| Pitch down                                   |                        |
| Pitch                                        | Y-Axis (mouse)         |
| Yaw left                                     |                        |
| Yaw right                                    |                        |
| Yaw                                          | X-Axis (mouse)         |
| E.S.P. (Toggle)                              | E                      |
| Turret Mouse Movement (Toggle)               | Q                      |
| Recenter Turret (Hold)                       | C                      |
| Turret Gyro Stabilization (Toggle)           | G                      |
| Turret Speed Limiter (Toggle)                | Left Shift             |
| Speed Limiter (rel.)                         | Mouse Wheel            |
| Velocity Limiter Increase (rel.)             | W                      |
| Velocity Limiter Decrease (rel.)             | S                      |
| Cycle fire mode (staggered / combined)       | V                      |
| Exit Remote Turret                           | Y                      |

## Flight - Power
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Toggle Power - All                           | U                      |
| Reset Priority                               |                        |
| Toggle Power - Thrusters                     |                        |
| Increase Priority - Thrusters                |                        |
| Toggle Power - Shields                       |                        |
| Increase Priority - Shields                  |                        |
| Toggle Power - Weapons                       |                        |
| Increase Priority - Weapons                  |                        |
| Increase Throttle                            |                        |
| Increase Throttle to Max                     |                        |
| Decrease Throttle                            |                        |
| Decrease Throttle to Min                     |                        |

## Flight - Radar
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Personal ID Broadcast System (Toggle)        |                        |
| Radar cycle range                            |                        |

## Flight - HUD
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| mobiGlass (Toggle)                           | F1                     |
| Scoreboard                                   | Tab                    |
| Map                                          | F2                     |
| Wipe Helmet Visor                            | Left Shift + Z         |

## Flight - Mining
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Mining Mode (Toggle)                         |                        |
| Fire Mining Laser (Toggle)                   |                        |
| Switch Mining Laser (Toggle)                 |                        |
| Increase Mining Laser Power                  | Mouse Wheel Up         |
| Decrease Mining Laser Power                  | Mouse Wheel Down       |
| Activate Mining Consumable 1                 | Left Alt + 1           |
| Activate Mining Consumable 2                 | Left Alt + 2           |
| Activate Mining Consumable 3                 | Left Alt + 3           |
| Jettison Cargo                               | Left Alt + T           |

## Flight - Target Hailing
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Hail Target                                  | 6                      |

