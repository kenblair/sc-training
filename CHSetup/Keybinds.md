# CH Setup for Star Citizen 3.10

Use a product such as CH Control Manager or JoyToKey to map buttons to "modifier" keys such as Right Alt and Right Control to provide alternative mappings to buttons when the modifier is held down.  If the binding includes (M1) that indicates you must hold the M1 modifier key or button down at the same time while (M2) is for the second modifier.

All bindings are shown with the default mouse and keyboard binding followed by a recommendation for a CH based setup. Recommendations are based on a FighterStick, ProThrottle and ProPedals setup with the most important and frequently used flight combat bindings on the right stick. The mini joystick on the ProThrottle is used for strafing with the 4-way hats providing additional bindings for power, shields, landing and the quantum drive. ProPedals are used for roll and controlling the speed limiter.

If you are using a dual joystick setup you can move the bindings on the throttle to your left joystick. If you have a reliable twist axis on the left joystick you may want to use it for strafing up and down or for a roll if you don't have ProPedals. Be wary of assigning the twist on a T-16000M as it's known to break.

# CH Control Manager / JoyToKey / TeamSpeak / etc.

Modifiers are set to a control key such as Alt, Ctrl or Shift. Star Citizen, CH Control Manager and JoyToKey can all specify the left or right Alt/Ctrl/Shift. The left version is more commonly used on the keyboard and in other games, the right can often be devoted to your joystick bindings without interfering with other normal keyboard and mouse bindings. 

Use push-to-talk for Discord, TeamSpeak, Ventrilo, Mumble, etc. with priority push-to-talk as an alternate binding for a command channel or a priority push-to-talk binding in applications like Discord.

## Modifiers
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Modifier Key 1 (M1)                          | Right Alt              | Button 4 (ProThrottle)
| Modifier Key 2 (M2)                          | Right Ctrl             | Button 3 (ProThrottle)

## Voice
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Push-to-Talk                                 |                        | Button 4 (FighterStick)
| Priority Push-to-Talk                        |                        | (M1) Button 4 (FighterStick)
| Mute Sound                                   |                        | (M2) Button 4 (FighterStick)

# Game Bindings

## Flight - Cockpit
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Eject                                        | Y                      | (M2) Button 9 (FighterStick)
| Exit Seat                                    | Y                      | (M2) Button 9 (FighterStick)
| Self Destruct                                | Backspace              |
| Emergency Exit Seat                          | Left Shift + U         |
| Increase Cooler Rate                         |                        | Button 5 (ProThrottle)
| Decrease Cooler Rate                         |                        | Button 7 (ProThrottle)
| Flight / Systems Ready                       | R                      | (M1) Button 4 (ProThrottle)
| Open/Close Doors (Toggle)                    | K                      | (M1) Button 10 (FighterStick)
| Lock/Unlock Doors (Toggle)                   | L                      | (M2) Button 10 (FighterStick)

## Flight - View
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Look left                                    |                        | POV Hat Up (FighterStick)
| Look right                                   |                        | POV Hat Right (FighterStick)
| Look left / right                            | X-Axis (mouse)         | X-Axis (FighterStick)
| Look up                                      |                        | POV Hat Up (FighterStick)
| Look down                                    |                        | POV Hat Down (FighterStick)
| Look up / down                               | Y-Axis (mouse)         | Y-Axis (FighterStick)
| Cycle camera view                            | F4                     | (M1) Button 11 (FighterStick)
| Cycle camera orbit mode                      |                        |
| Zoom in (3rd person)                         | Mouse Wheel Up         | Right Pedal (ProPedals)
| Zoom out (3rd person)                        | Mouse Wheel Down       | Left Pedal (ProPedals)
| Freelook (Hold)                              | Z                      | Button 4 (ProThrottle)
| Dynamic zoom in and out (rel.)               | Left Alt + Mouse Wheel | (M1) Left / Right Pedal (ProPedals)
| Dynamic zoom in (rel.)                       |                        |
| Dynamic Zoom Toggle (abs.)                   |                        |
| Look behind                                  |                        | Button 11 (FighterStick)

## Flight - Movement

### Directional
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Pitch                                        | Y-Axis (mouse)         | Y-Axis (FighterStick)
| Yaw                                          | X-Axis (mouse)         | X-Axis (FighterStick)
| Cycle mouse mode                             |                        |
| Roll                                         |                        | Rudder (ProPedals)
| Swap Yaw / Roll (Toggle)                     |                        |
| Lock Pitch / Yaw Movement (Toggle, Hold)     | Right Shift            |
| E.S.P. (Toggle)                              |                        | (M1) Button 12 (FighterStick)

### Limiters
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Speed Limiter (rel.)                         | Mouse Wheel            | Left / Right Pedal (ProPedals)
| Acceleration Limiter Up / Down (rel.)        | Right Alt + Mouse Wheel| (M1) Left / Right Pedal (ProPedals)
| Speed Limiter (Toggle)                       |                        | Button 12 (FighterStick)
| G-force safety (Toggle)                      |                        | (M2) Button 12 (FighterStick)

### Engines & Thrusters
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Afterburner                                  | Left Shift             | Button 5 (ProThrottle)
| Spacebrake                                   | X                      | Button 7 (ProThrottle)
| Throttle forward / back                      |                        | Z-Axis (ProThrottle)
| Strafe up / down                             |                        | Y-Axis (ProThrottle)
| Strafe left / right                          |                        | X-Axis (ProThrottle)
| Decoupled mode (Toggle)                      | V                      | Button 8 (ProThrottle)
| VTOL (Toggle)                                | J                      | (M1) Button 8 (ProThrottle)
| Cruise Control (Toggle)                      | C                      | (M2) Button 8 (ProThrottle)

### Quantum & Landing
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Quantum Travel System (Toggle)               | B                      | Button 14 (ProThrottle)
| Quantum Drive (Hold)                         | B                      | Button 14 (ProThrottle)
| Landing System (Toggle)                      | N                      | Button 13 (ProThrottle)
| Autoland (Hold)                              | N                      | Button 13 (ProThrottle)

## Flight - Targeting
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Lock / Unlock Selected Target                | 1                      | Button 5 (FighterStick)
| Lock Nearest Attacker                        | 2                      | Button 7 (FighterStick)
| Lock Next Pinned Target                      | 4                      | Button 6 (FighterStick)
| Lock Previous Pinned Target                  | 5                      | Button 8 (FighterStick)
| Unlock Locked Target                         |                        | (M1) Button 7 (FighterStick)

### Pinning
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Pin / Unpin Selected Target                  | 3                      | (M1) Button 5 (FighterStick)
| Pin Index 1 - Lock / Unlock Pinned Target    | Left Alt + 1           | (M1) Button 16 (ProThrottle)
| Pin Index 1 - Pin / Unpin Selected Target    | Left Alt + 1           | (M2) Button 16 (ProThrottle)
| Pin Index 2 - Lock / Unlock Pinned Target    | Left Alt + 2           | (M1) Button 13 (ProThrottle)
| Pin Index 2 - Pin / Unpin Selected Target    | Left Alt + 2           | (M2) Button 13 (ProThrottle)
| Pin Index 3 - Lock / Unlock Pinned Target    | Left Alt + 3           | (M1) Button 14 (ProThrottle)
| Pin Index 3 - Pin / Unpin Selected Target    | Left Alt + 3           | (M2) Button 14 (ProThrottle)
| Remove All Pinned Targets                    | 0                      | (M1) Button 15 (ProThrottle)

### Selection
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Hostiles - Select Next Target in View        |                        | (M1) Button 6 (FighterStick)
| Hostiles - Select Previous Target in View    |                        | (M1) Button 8 (FighterStick)
| Friendlies - Select Next Target in View      |                        | (M2) Button 6 (FighterStick)
| Friendlies - Select Previous Target in View  |                        | (M2) Button 8 (FighterStick)
| All Targets - Select Next Target in View     |                        | (M2) Button 5 (FighterStick)
| All Targets - Select Previous Target in View |                        | (M2) Button 7 (FighterStick)
| Reset Selection                              |                        | Button 7 (FighterStick)

### Sub-Target
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Lock Next Sub-Target                         |                        | (M2) Button 5 (FighterStick)
| Lock Previous Sub-Target                     |                        | (M2) Button 7 (FighterStick)
| Reset Sub-Target                             |                        | (M2) Button 7 (FighterStick) (if hold works)

### Utility
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Look at Locked Target (Toggle, Hold)         |                        | Button 16 (FighterStick)
| Auto Zoom on Selected Target (Toggle, Hold)  |                        | (M1) Button 16 (FighterStick)
| Look Ahead (Toggle)                          | Left Alt + R           | (M2) Button 16 (FighterStick)

### Scanning
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Scanning Mode (Toggle)                       | Tab                    | (M2) Button 3 (FighterStick)
| Activate Scanning                            | Button 1 (mouse)       | Button 1 (FighterStick)
| Scanning Radar Ping                          | Button 2 (mouse)       | Button 2 (FighterStick)
| Scanning Increase Radar Angle                | Mouse Wheel Up         | Right Pedal (ProPedals)
| Scanning Decrease Radar Angle                | Mouse Wheel Down       | Left Pedal (ProPedals)

## Flight - Weapons
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Fire Weapon Group 1                          | Button 1 (mouse)       | Button 1 (FighterStick)
| Fire Weapon Group 2                          | Button 2 (mouse)       | Button 3 (FighterStick)
| Cycle Weapon Ammo                            |                        |
| Cycle Weapon Ammo (Back)                     |                        |
| Cycle Gimbal Assist                          | R                      | Button 14 (FighterStick)
| Switch Flight Lead / Lag Reticle (Toggle)    |                        | (M1) Button 14 (FighterStick)
| Gimbal Mode (Toggle, Hold)                   |                        | (M2) Button 14 (FighterStick)
| Acquire missile lock (Tap)                   | Button 3 (mouse)       | Button 2 (FighterStick)
| Release missile lock (Tap)                   |                        | Button 2 (FighterStick)
| Launch missile (Hold)                        | Button 3 (mouse)       | Button 2 (FighterStick)

## Flight - Defensive
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Launch countermeasure                        | G                      | Button 13 (FighterStick)
| Cycle countermeasure ammo                    | H                      | (M1) Button 13 (FighterStick)
| Cycle countermeasure ammo (Back)             |                        | (M2) Button 13 (FighterStick)
| Shield raise level front                     | Numpad 8               | POV Hat Up (ProThrottle)
| Shield raise level back                      | Numpad 2               | POV Hat Down (ProThrottle)
| Shield raise level left                      | Numpad 4               | POV Hat Left (ProThrottle)
| Shield raise level right                     | Numpad 6               | POV Hat Right (ProThottle)
| Shield raise level top                       | Numpad 7               | (M1) POV Hat Up (ProThrottle)
| Shield raise level bottom                    | Numpad 9               | (M1) POV Hat Down (ProThrottle)
| Shield reset levels                          | Numpad 5               | (M1) POV Hat Left (ProThrottle)

## Turrets
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Pitch                                        | Y-Axis (mouse)         | Y-Axis (FighterStick)
| Yaw                                          | X-Axis (mouse)         | X-Axis (FighterStick)
| Turret Speed Limiter (Toggle)                | Left Shift             | Button 12 (FighterStick)
| E.S.P. (Toggle)                              | E                      | (M1) Button 12 (FighterStick)
| Turret Gyro Stabilization (Toggle)           | G                      | (M2) Button 12 (FighterStick)
| Turret Mouse Movement (Toggle)               | Q                      |
| Recenter Turret (Hold)                       | C                      |
| Speed Limiter (rel.)                         | Mouse Wheel            | Left / Right Pedal (ProPedals)
| Velocity Limiter Increase (rel.)             | W                      |
| Velocity Limiter Decrease (rel.)             | S                      |
| Cycle fire mode (staggered / combined)       | V                      |
| Exit Remote Turret                           | Y                      |

## Flight - Power
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Increase Priority - Shields                  |                        | Button 12 (FighterStick)
| Increase Priority - Thrusters                |                        | Button 9 (FighterStick)
| Increase Priority - Weapons                  |                        | Button 10 (FighterStick)
| Reset Priority                               |                        | Button 11 (FighterStick)
| Toggle Power - Shields                       |                        | (M1) Button 12 (FighterStick)
| Toggle Power - Thrusters                     |                        | (M1) Button 9 (FighterStick)
| Toggle Power - Weapons                       |                        | (M1) Button 10 (FighterStick)
| Toggle Power - All                           | U                      | (M1) Button 11 (FighterStick)
| Increase Throttle                            |                        |
| Increase Throttle to Max                     |                        |
| Decrease Throttle                            |                        |
| Decrease Throttle to Min                     |                        |

## Flight - Radar
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Personal ID Broadcast System (Toggle)        |                        | Button 15 (ProThrottle)
| Radar cycle range                            |                        | Button 16 (ProThrottle)

## Flight - HUD
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Map                                          | F2                     | Button 9 (FighterStick)
| mobiGlass (Toggle)                           | F1                     | (M1) Button 9 (FighterStick)
| Wipe Helmet Visor                            | Left Shift + Z         | (M2) Button 9 (FighterStick)

## Flight - Mining
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Mining Mode (Toggle)                         |                        | (M2) Button 3 (FighterStick)
| Fire Mining Laser (Toggle)                   |                        | Button 1 (FighterStick)
| Switch Mining Laser (Toggle)                 |                        | Button 2 (FighterStick)
| Increase Mining Laser Power                  | Mouse Wheel Up         | Right Pedal (ProPedals)
| Decrease Mining Laser Power                  | Mouse Wheel Down       | Left Pedal (ProPedals)
| Activate Mining Consumable 1                 | Left Alt + 1           | Button 6 (ProThrottle)
| Activate Mining Consumable 2                 | Left Alt + 2           | (M1) Button 6 (ProThrottle)
| Activate Mining Consumable 3                 | Left Alt + 3           | (M2) Button 6 (ProThrottle)
| Jettison Cargo                               | Left Alt + T           | (M2) Button 8 (ProThrottle)

## Flight - Target Hailing
| Name                                         | Default                | Modifier, Button/Axis, Input               |
|----------------------------------------------|------------------------|--------------------------------------------|
| Hail Target                                  | 6                      | (M1) Button 4 (ProThrottle)
