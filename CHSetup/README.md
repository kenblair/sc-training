# Overview

A solid mid-level HOTAS set suitable for flight simulators with optional heavy duty desk mount from Monstertech. A CH setup will cost considerably more than an entry level dual T-16000M at around $300 for the stick and throttle alone. The Pro Pedals are entirely optional however the additional analog axis can be bound to roll or yaw and the pedals can easily be mapped to a mouse wheel with JoyToKey for even more options.

# Pricing

| Device                  | Price        |            
|-------------------------|--------------|              
| CH Fighterstick         | $130 - $160  |
| CH Pro Throttle         | $130 - $160  |
| CH Pro Pedals           | $130 - $160  |
| Monstertech Table Mount | $99 each     |
| Total                   | $390 - $480  |
| With desk mounts        | $590 - $680  |

Expect to pay approximately $400 for a full set, $600 with Monstertech desk mounts.

# Fighterstick

The CH Fighterstick only comes in a right handed grip and does not have a twist axis. The stick is slightly on the larger side and may be too big for small hands.

# ProThrottle

The CH ProThrottle comes with an analog mini joystick that is of high quality. It does have trouble returning to center accurately and needs a fairly large dead zone.

## Blank Binding Templates

![CH Fighterstick Binding Template](CHFighterstickBlank.png)
![CH Pro Throttle Binding Template](CHProThrottleBlank.png)

# CH Control Manager Setup

If you're using CH products including the FighterStick, CombatStick, FlightStick, ProThrottle or ProPedals you can configure your devices with the CH Control Manager software. This will allow you to change settings that are not available in Star Citizen to get a reliable HOTAS, dual stick or combination setup.

# Installation

Download the control manager from the [CH website](http://www.chproducts.com/13-29620-Downloads-and-Community-Links.php.html) where it may be listed as "optional control manager programming software".

# Overview

In general there's a few steps to follow when setting up any device in the control manager software:

1. Add the device(s) you want to map/use with the "+" icon.
2. Configure your devices with the software.
3. Save the mapping to a file with the "Save" button.
4. IMPORTANT: You MUST click "Download" to actually ACTIVATE the mapping in your devices. Until you click "Download" nothing has changed in your devices!

EVERY time you click "Download" your devices will STOP WORKING in Star Citizen. If you want to change something you need to leave Star Citizen completely, make the change, download and then launch Star Citizen again. This makes testing changes difficult but I've provided a reasonable starting point with the guide below.

# Basics

This brief guide will cover a few basics that are commonly desired in HOTAS or dual stick setups:

1. Changing an axis to centered.
2. Inverting an axis.
3. Adding a dead zone to an axis.
4. Changing the response curve of an axis.

## Centered Axis

When an axis is "centered" it has a middle where there can be a dead zone. The analog sticks on a typical PS or XBOX controller are "centered" and this is typical of joysticks.

When an axis is NOT "centered" it has no middle, think of the triggers on a PS or XBOX controller. They only go one direction and there is no "middle". This is typical for a throttle, all the way back is 0 (no throttle) and all the way forward is 100 (full throttle).

## Inverted Axis

You may notice that in game applying forward throttle sends you backwards and applying reverse throttle sends you forwards. This is a situation where you want to invert the axis because Star Citizen is interpreting it as the opposite of what you want.

Likewise on a Y axis if you find that up is going down and down is going up and you'd like to reverse those simply invert/reverse the axis in the control manager.

## Dead Zone

On a centered axis such as a typical joystick X & Y axis there is a middle that represents 0. The dead zone expands the "middle" area to make it larger. If you find that when you leave your joystick at center you cannot get your ship to stop drifting in one direction or the other you may need to increase the dead zone. The downside of a dead zone is you need to move the stick a little further from center before you see any movement. The upside is when you release the controls and let it center the ship stops moving because the joystick is at 0.

## Response Curve

By default the response curve is linear. Moving the stick a certain distance always gets you the same change in input. By changing the response curve we can change how "sensitive" the joystick, throttle or rudders feel at different points. By clicking the down arrow we're changing the curve so that the "middle" area is less sensitive and the edges are more sensitive.

What this means is when you start to move your stick from center the change is more gradual, you have more fine grained control. As you move the stick further from center the sensitivity starts to increase so you can still easily hit full left/right/up/down/etc. but have more control in the middle of the stick.

If you click up on the curve buttons you will do the opposite. The stick will be more sensitive and react faster in the middle while becoming less sensitive and affording more control at the edges (e.g. when pushing far to the right/left/up/down).

This is entirely personal preference. I give most axis a slight curve by clicking the down arrow 1 or 2 times. When I'm landing or making small aim changes I have more control and can make small adjustments, when I'm dogfighting and need a sharp turn I can still throw the stick into the turn and turn just as fast.

# Add Devices to Mapping

![picture](images/AddSticksToManager.gif)

Add all the devices you want to configure to the mapping. In these examples we're configuring a HOTAS with pedals setup.

IMPORTANT: Order is very important, this is the order Star Citizen will see the devices. Choose your joystick first, then throttle, then pedals.

# FighterStick

![picture](images/FighterStickSettings.gif)

There's not much we need to change with the FighterStick. Leave the X & Y axis as typical centered axis. You may apply a dead zone here if you wish, I have not needed it. If you do apply a dead zone it should be small such as 1 or 2.

# ProThrottle

![picture](images/ProThrottleSettings.gif)

1. Invert the throttle, Star Citizen will have it backwards by default.
2. Optionally choose a "centered" throttle if you want the option to pull the throttle backwards to reverse the ship.
3. If you use a "centered" throttle give yourself a large dead zone to make it easy to find 0 throttle: 10 - 20.
4. Optionally change the curve, 1-3 down arrow clicks. This will make the throttle less sensitive at low speeds and therefore easier to make small adjustments.

I generally use the mini joystick for thrusters. I've found the mini stick on the ProThrottle has trouble returning to center causing the ship to drift so I gave it a big dead zone of 3 and adjusted the response curve as usual.

If you use the mini stick Y axis for thrusters up/down you may want to invert it. By default SC will use up as down and down as up, I prefer the opposite so I checked "reverse" on the Y axis.

# ProPedals

![picture](images/ProPedalsSettings.gif)

Not much to do with the pedals, I haven't figured out what to use the X/Y axis for. The Z axis which is typically rudder can be assigned to roll (or yaw if you use your stick for roll). I did not need to invert it, I did give it a small dead zone and changed the curve.

# Save Mapping & Download

![picture](images/SaveSettings.gif)

Save the mapping so you have it for future use. Click "Download" to actually APPLY the mapping.

# Keybinds

Check the keybinds.md file for recommended bindings for different CH setups.
